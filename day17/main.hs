class Functor w => Comonad w where
    extract :: w a -> a
    duplicate :: w a -> w (w a)
    extend :: (w a -> b) -> w a -> w b
    extend f = fmap f . duplicate


data Axis a = Axis [a] a [a]

newtype Grid a = Grid { getAxes :: Axis (Axis (Axis a)) }

data State = Active | Inactive deriving (Eq)


left :: Axis a -> Axis a
left (Axis (x':l) x r) = Axis l x' (x:r)


right :: Axis a -> Axis a
right (Axis l x (x':r)) = Axis (x:l) x' r


instance Functor Axis where
    fmap f (Axis l x r) = Axis (fmap f l) (f x) (fmap f r)


instance Comonad Axis where
    extract (Axis _ x _) = x
    duplicate a = Axis (tail $ iterate left a) a (tail $ iterate right a)


instance Functor Grid where
--  fmap :: (a -> b) -> Axis (Axis (Axis a)) -> Axis (Axis (Axis b))
    fmap f = Grid . (fmap . fmap . fmap) f . getAxes


instance Comonad Grid where
    extract w = (extract . extract . extract) $ getAxes w
--  duplicate :: Axis (Axis (Axis a)) -> Axis (Axis (Axis (Axis (Axis (Axis a)))))
--  WTF?
--  this looks stupid, but works
    duplicate = fmap Grid . Grid . (fmap . fmap . fmap . fmap) duplicate . (fmap . fmap) duplicate . duplicate . getAxes




neighbors1d :: Axis a -> [a]
neighbors1d (Axis (l:_) _ (r:_)) = [l, r]


neighbors2d :: Axis (Axis a) -> [a]
neighbors2d (Axis (l:_) x (r:_)) = (extract l):(neighbors1d l) ++ neighbors1d x ++ (extract r):(neighbors1d r)


neighbors3d :: Axis (Axis (Axis a)) -> [a]
neighbors3d (Axis (l:_) x (r:_)) = (extract $ extract l):(neighbors2d l) ++ neighbors2d x ++ (extract $ extract r):(neighbors2d r)


neighbors :: Grid a -> [a]
neighbors = neighbors3d . getAxes


evolve :: Grid State -> State
evolve g =
    let active_neighbors = length $ filter (==Active) (neighbors g)
    in case (extract g) of
        Active -> if active_neighbors == 2 || active_neighbors == 3 then Active else Inactive
        Inactive -> if active_neighbors == 3 then Active else Inactive



default_axis :: Axis State
default_axis = Axis (repeat Inactive) Inactive (repeat Inactive)


default_plane :: Axis (Axis State)
default_plane = Axis (repeat default_axis) default_axis (repeat default_axis)


axis1d :: [State] -> Axis State
axis1d (x:xs) = Axis (repeat Inactive) x (xs ++ repeat Inactive)


axis2d :: [[State]] -> Axis (Axis State)
axis2d (l:ls) = Axis (repeat default_axis) (axis1d l) (map axis1d ls ++ repeat default_axis)


stateFromChar :: Char -> State
stateFromChar '#' = Active
stateFromChar '.' = Inactive





main :: IO ()
main = print 1
