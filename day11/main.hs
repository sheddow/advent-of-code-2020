import Control.Monad
import Data.Maybe
import Data.List

import Data.Sequence hiding (zip, filter, Empty, length, drop)
import qualified Data.Sequence as Seq


data Seat = Floor | Empty | Occupied deriving (Eq, Show)


seatFromChar :: Char -> Seat
seatFromChar '#' = Occupied
seatFromChar 'L' = Empty
seatFromChar '.' = Floor


type Grid = Seq (Seq Seat)


countOccupied :: Grid -> Int
countOccupied grid = sum $ fmap (Seq.length . Seq.filter (==Occupied)) grid


neighbors :: Grid -> Int -> Int -> [Seat]
neighbors grid row col =
    let row_offsets = [-1..1]
        col_offsets = [-1..1]
        indices = [(row+r, col+c) | r <- row_offsets, c <- col_offsets, (r, c) /= (0, 0)]
    in catMaybes [grid !? r >>= (!? c) | (r, c) <- indices]


occupiedNeighbors :: Grid -> Int -> Int -> Int
occupiedNeighbors grid row col = length $ filter (==Occupied) (neighbors grid row col)


applyRule :: Grid -> Grid
applyRule grid =
    let f row col seat = if seat == Empty && occupiedNeighbors grid row col == 0 then
            Occupied
        else if seat == Occupied && occupiedNeighbors grid row col >= 4 then
            Empty
        else
            seat
    in mapWithIndex (\rowidx row -> mapWithIndex (\colidx seat -> f rowidx colidx seat) row) grid


applyUntilStable :: Eq a => (a -> a) -> a -> a
applyUntilStable f x =
    let x' = f x
    in if x' == x then
        x
    else
        applyUntilStable f x'


visualNeighbors :: Grid -> Int -> Int -> [Seat]
visualNeighbors grid row col =
    let directions = [(r, c) | r <- [-1..1], c <- [-1..1], (r, c) /= (0, 0)]
        lookupGrid row col = grid !? row >>= (!? col)
        directionIter rdiff cdiff = tail $ zip [row, (row+rdiff)..] [col, (col+cdiff)..]
        firstInDirection rdiff cdiff = join $ find (/= Just Floor) $ map (uncurry lookupGrid) (directionIter rdiff cdiff)
    in catMaybes [firstInDirection r c | (r, c) <- directions]


occupiedNeighbors' :: Grid -> Int -> Int -> Int
occupiedNeighbors' grid row col = length $ filter (==Occupied) (visualNeighbors grid row col)


applyRule' :: Grid -> Grid
applyRule' grid =
    let f row col seat = if seat == Empty && occupiedNeighbors' grid row col == 0 then
            Occupied
        else if seat == Occupied && occupiedNeighbors' grid row col >= 5 then
            Empty
        else
            seat
    in mapWithIndex (\rowidx row -> mapWithIndex (\colidx seat -> f rowidx colidx seat) row) grid



main :: IO ()
main = do
    s <- readFile "input.txt"
    let grid = fromList $ map (fromList . map seatFromChar) (lines s) :: Seq (Seq Seat)
    print $ countOccupied (applyUntilStable applyRule grid)
    print $ countOccupied (applyUntilStable applyRule' grid)
