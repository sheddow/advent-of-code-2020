import Lib

import qualified Data.Map.Strict as Map

import Test.Hspec
import Text.Parsec


example_string :: String
example_string = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm"

example_string2 :: String
example_string2 = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm\n\nrandom123"

example_map :: Map.Map String String
example_map = Map.fromList [("byr","1937"),("cid","147"),("ecl","gry"),("eyr","2020"),("hcl","#fffffd"),("hgt","183cm"),("iyr","2017"),("pid","860033327")]


spec :: Spec
spec = do
    describe "parseField" $ do
        it "works" $ parse parseField "" "ecl:gry" `shouldBe` Right ("ecl", "gry")
    describe "parsePassport" $ do
        it "works" $ (parse parsePassport "" example_string) `shouldBe` Right example_map
    describe "getPassports" $ do
        it "works" $ fmap (fmap length . getPassports) (readFile "test_input.txt") `shouldReturn` Right 4
--        it "works2" $ (parse parsePassport "" example_string2) `shouldBe` Right example_map
--    describe "parseManyPassports" $ do
--        it "works" $ fmap (parse parseManyPassports "" . init) (readFile "test_input.txt") `shouldReturn` Right []


main :: IO ()
main = hspec spec
