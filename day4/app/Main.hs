module Main where

import Lib

main :: IO ()
main = do
    s <- readFile "input.txt"
    let (Right passports) = getPassports s
    let valid_passports = filter isValid passports
    print $ length valid_passports
    let very_valid_passports = filter isStrictlyValid passports
    print $ length very_valid_passports
