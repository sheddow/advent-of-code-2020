module Lib
    ( Passport
    , parseField
    , parsePassport
    , getPassports
    , isValid
    , isStrictlyValid
    ) where

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

import Data.List.Split (splitOn)
import Data.Char (isSpace)
import Text.Read (readMaybe)

import Text.Regex.TDFA

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String (Parser)


type Passport = Map.Map String String


parseField :: Parser (String, String)
parseField = do
    key <- count 3 letter
    char ':'
    value <- many1 (satisfy (not . isSpace))
    return (key, value)


parsePassport :: Parser Passport
parsePassport = do
    fields <- many1 (between spaces spaces parseField)
    return $ Map.fromList fields


getPassports :: String -> Either ParseError [Passport]
getPassports s = mapM (parse parsePassport "") (splitOn "\n\n" s)


isValid :: Passport -> Bool
isValid m =
    let s1 = Set.fromList ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"] -- cid optional
        s2 = Set.fromList (Map.keys m)
    in s1 `Set.isSubsetOf` s2


validYear :: Int -> Int -> String -> Bool
validYear min' max' s = maybe False (\x -> x >= min' && x <= max') (readMaybe s)


validHeight :: String -> Bool
validHeight s =
    let (_, m, after) = s =~ "^[0-9]+" :: (String, String, String)
    in case after of
        "cm" -> maybe False (\x -> 150 <= x && x <= 193) (readMaybe m)
        "in" -> maybe False (\x -> 59 <= x && x <= 76) (readMaybe m)
        _ -> False


validHcl :: String -> Bool
validHcl s = s =~ "^#[0-9a-f]{6}$"


validEcl :: String -> Bool
validEcl s = s `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]


validPid :: String -> Bool
validPid s = s =~ "^[0-9]{9}$"


validators :: Map.Map String (String -> Bool)
validators = Map.fromList [
    ("byr", validYear 1920 2002),
    ("iyr", validYear 2010 2020),
    ("eyr", validYear 2020 2030),
    ( "hgt", validHeight),
    ("hcl", validHcl),
    ("ecl", validEcl),
    ("pid", validPid)]


isStrictlyValid :: Passport -> Bool
isStrictlyValid passport = 
    foldr (&&) True $ Map.mapWithKey (\key isValid -> maybe False isValid (Map.lookup key passport)) validators
