import Text.Parsec
import Text.Parsec.String


data Expr = Val Int | Add Expr Expr | Mul Expr Expr deriving Show


unwrap :: Show a => Either a b -> b
unwrap (Right x) = x
unwrap (Left x) = error $ show x


eval :: Expr -> Int
eval (Val x) = x
eval (Add a b) = (eval a) + (eval b)
eval (Mul a b) = (eval a) * (eval b)


mulop :: Parser (Expr -> Expr -> Expr)
mulop = do
    spaces >> char '*' >> spaces
    return Mul


addop :: Parser (Expr -> Expr -> Expr)
addop = do
    spaces >> char '+' >> spaces
    return Add


expr :: Parser Expr
expr = term `chainl1` (try addop <|> mulop)

term :: Parser Expr
term = fmap (Val . read) (many1 digit) <|> between (char '(') (char ')') expr


expr' :: Parser Expr
expr' = factor' `chainl1` try mulop


factor' :: Parser Expr
factor' = term' `chainl1` try addop


term' :: Parser Expr
term' = fmap (Val . read) (many1 digit) <|> between (char '(') (char ')') expr'


main :: IO ()
main = do
    s <- readFile "input.txt"
    print $ sum $ map (eval . unwrap . parse expr "") (lines s)
    print $ sum $ map (eval . unwrap . parse expr' "") (lines s)
