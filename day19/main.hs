import Data.List.Split

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map


type RulesMap = Map Int (Either Char [[Int]])


data Rule = Lit Char | Or [Rule] | And [Rule] deriving Show


parseInt :: Parser Int
parseInt = fmap read $ many1 digit


parseRule :: Parser (Int, Either Char [[Int]])
parseRule = do
    idx <- parseInt
    char ':'
    spaces
    val <- fmap Left (between (char '"') (char '"') letter) <|> fmap Right (sepBy1 (sepEndBy1 parseInt space) (string "| "))
    eof
    return (idx, val)



makeRule :: RulesMap -> Int -> Rule
makeRule m x =
    case Map.lookup x m of
        Just (Left c) -> Lit c
        Just (Right l) -> Or $ map (And . map (makeRule m)) l
        Nothing -> error "rule doesn't exist in map"


unwrap :: Show a => Either a b -> b
unwrap (Right x) = x
unwrap (Left x) = error $ show x


strToRulesMap :: String -> RulesMap
strToRulesMap s = Map.fromList $ map (unwrap . parse parseRule "") (lines s)


matchesRule :: String -> Rule -> (String, Bool)
matchesRule (c1:s) (Lit c2) = (s, c1 == c2)
matchesRule s (And []) = (s, True)
matchesRule s (And (r:l)) =
    let (s', m) = matchesRule s r
    in if m then matchesRule s' (And l) else (s', False)
matchesRule s (Or []) = (s, False)
matchesRule s (Or (r:l)) =
    let (s', m) = matchesRule s r
    in if m then (s', True) else matchesRule s (Or l)


matchesFully :: Rule -> String -> Bool
matchesFully r s = matchesRule s r == ("", True)


--matchesRulesMap :: String -> Int -> RulesMap -> (String, Bool)
--matchesRulesMap s idx m = -- TODO


main :: IO ()
main = do
    s <- readFile "input.txt"
    let (rules_str:msg:[]) = splitOn "\n\n" s
    let rule = makeRule (strToRulesMap rules_str) 0

    print $ length $ filter (matchesFully rule) (lines msg)
