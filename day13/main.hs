import Data.List.Split
import Data.List
import Data.Function (on)


getEarliestBus :: [Int] -> Int -> (Int, Int)
getEarliestBus l ts = 
    let ts' = fromIntegral ts
        f i = i * (ceiling (ts' / (fromIntegral i)))
        i = minimumBy (compare `on` f) l
    in (i, f i)


eea :: Int -> Int -> (Int, Int)
eea a 0 = (1, 0)
eea a b = (t, s - q * t)
    where (q, r) = quotRem a b
          (s, t) = eea b r


modularInv :: Int -> Int -> Int
modularInv a b = fst $ eea a b


solveSystemOfCongruences :: [(Int, Int)] -> Int
solveSystemOfCongruences l =
    let bign = product $ map snd l
        x = sum $ map (\(a, n) -> a * (bign `div` n) * (modularInv (bign `div` n) n)) l
    in snd $ divMod x bign


main :: IO ()
main = do
    s <- readFile "input.txt"
    let (l1:l2:_) = lines s

    let timestamp = read l1
    let ids = map read $ filter (/="x") (splitOn "," l2)

    let (i, departing) = getEarliestBus ids timestamp
    let wait = departing - timestamp

    print $ i*wait

    let system_of_congruences = map (\(a, b) -> (a, read b)) $ filter ((/="x") . snd) $ zip [0,-1..] (splitOn "," l2)

    print $ solveSystemOfCongruences system_of_congruences
