import Test.Hspec

import Lib


spec :: Spec
spec = do
    describe "strToRow" $ do
        it "works" $ strToRow "FBFBBFF" `shouldBe` 44
    describe "strToCol" $ do
        it "works" $ strToCol "RLR" `shouldBe` 5
    describe "getSeatID" $ do
        it "works" $ getSeatID "FBFBBFFRLR" `shouldBe` 357


main :: IO ()
main = hspec spec
