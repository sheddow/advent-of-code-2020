module Main where

import Data.List

import Lib

main :: IO ()
main = do
    s <- readFile "input.txt"
    let passes = lines s
    let seatIDs = map getSeatID passes
    let max' = maximum seatIDs
    print max'
    let min' = minimum seatIDs

    let a = [min'..max']
    let missing = a \\ (sort seatIDs)
    print missing
