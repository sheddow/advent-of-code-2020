{-# LANGUAGE MultiWayIf #-}

module Lib
    ( strToRow
    , strToCol
    , getSeatID
    ) where


strToRow :: String -> Int
strToRow = strToBin 'F' 'B'


strToCol :: String -> Int
strToCol = strToBin 'L' 'R'


strToBin :: Char -> Char -> String -> Int
strToBin c1 c2 s =
    let f c = if | c == c1 -> 0
                 | c == c2 -> 1
                 | otherwise -> error "oops"
    in sum $ zipWith (*) (map f (reverse s)) (iterate (*2) 1)


getSeatID :: String -> Int
getSeatID s =
    let row = strToRow $ take 7 s
        col = strToCol $ drop 7 s
    in row*8 + col
