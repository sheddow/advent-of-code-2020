import Data.Complex
import Data.Function (on)

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String (Parser)


data Direction = North | East | South | West deriving (Enum, Show)

data Instruction = Forward Int | Dir Direction Int | Turn Int

data Ship = Ship Direction Int Int deriving Show


move :: Ship -> Instruction -> Ship
move (Ship d x y) (Forward n) =
    let (dx, dy) = moveInDirection d n
    in (Ship d (x+dx) (y+dy))
move (Ship d x y) (Dir dir n) =
    let (dx, dy) = moveInDirection dir n
    in (Ship d (x+dx) (y+dy))
move (Ship d x y) (Turn n) =
    let new_dir = toEnum $ ((fromEnum d) + n) `rem` 4
    in (Ship new_dir x y)


moveInDirection :: Direction -> Int -> (Int, Int)
moveInDirection North d = (0, d)
moveInDirection South d = (0, -d)
moveInDirection East d = (d, 0)
moveInDirection West d = (-d, 0)


parseNum :: Parser Int
parseNum = fmap read (many1 digit)


parseInstruction :: Parser Instruction
parseInstruction = do
    c <- upper
    case c of
        'N' -> fmap (Dir North) parseNum
        'S' -> fmap (Dir South) parseNum
        'E' -> fmap (Dir East) parseNum
        'W' -> fmap (Dir West) parseNum
        'F' -> fmap Forward parseNum
        'R' -> fmap (Turn . (`div` 90)) parseNum
        'L' -> fmap (Turn . (4-) . (`div` 90)) parseNum


data Waypoint = Waypoint Int Int deriving Show


move' :: (Ship, Waypoint) -> Instruction -> (Ship, Waypoint)
move' (s, (Waypoint x y)) (Dir d n) =
    let (dx, dy) = moveInDirection d n
    in (s, (Waypoint (x+dx) (y+dy)))
move' (Ship d sx sy, w@(Waypoint wx wy)) (Forward n) =
    (Ship d (sx + n*wx) (sy + n*wy), w)
move' (s, (Waypoint wx wy)) (Turn n) =
    let rotation = case n of
            0 -> 1 :+ 0
            1 -> 0 :+ (-1)
            2 -> (-1) :+ 0
            3 -> 0 :+ 1
            _ -> error "oops"
        waypoint = ((:+) `on` fromIntegral) wx wy
        r = waypoint * rotation
    in (s, Waypoint (round $ realPart r) (round $ imagPart r))
    



main :: IO ()
main = do
    s <- readFile "input.txt"
    let unwrap (Right x) = x
    let instructions = map (unwrap . parse parseInstruction "") (lines s)

    let moves = map (flip move) instructions
    let supermove = foldr1 (flip (.)) moves

    let (Ship _ x y) = supermove (Ship East 0 0)

    print $ (abs x) + (abs y)


    let moves' = map (flip move') instructions
    let supermove' = foldr1 (flip (.)) moves'
    let (Ship _ x' y', _) = supermove' ((Ship East 0 0), (Waypoint 10 1))

    print $ (abs x') + (abs y')
