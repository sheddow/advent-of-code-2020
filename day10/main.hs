import Control.Monad
import Data.Function
import Data.List


joltageDifferences :: [Int] -> [Int]
joltageDifferences l =
    let m = maximum l
        sorted_l = 0 : (sort l) ++ [m + 3]
    in zipWith (-) (drop 1 sorted_l) sorted_l 


--getJoltArrangements :: [Int] -> Int -> [[Int]]
--getJoltArrangements numbers x =



next :: [Int] -> Int -> [Int]
next l x = filter (\y -> y > x && y <= (x+3)) l


getArrangements :: [Int] -> Int -> [[Int]]
getArrangements numbers start =
    if start == maximum numbers then
        [[]]
    else
        let l = next numbers start
            arrangements = join $ map (getArrangements numbers) l
        in map (start:) arrangements


memoize :: (Int -> a) -> (Int -> a)
memoize f = (map f [0..] !!)

getArrangements' :: [Int] -> (Int -> [[Int]]) -> Int -> [[Int]]
getArrangements' numbers f start =
    if start == maximum numbers then
        [[]]
    else
        let l = next numbers start
            arrangements = join $ map f l
        in map (start:) arrangements

arrangementsMemoized :: [Int] -> Int -> [[Int]]
arrangementsMemoized numbers = fix (memoize . (getArrangements' numbers))


tribonacci :: [Int]
tribonacci = 0 : 0 : 1 : zipWith3 (\a b c -> a + b + c) tribonacci (drop 1 tribonacci) (drop 2 tribonacci)


-- for a series of n consecutive numbers
-- starting at 0, the adapter can jump to 1, 2 or 3.
-- the possible combinations f(n) starting at 0 is therefore
-- f(n-1) + f(n-2) + f(n-3), ie. the tribonacci sequence
getNumOfArrangements :: [Int] -> Int
getNumOfArrangements numbers =
    let d = joltageDifferences numbers
        groups = group d
        f gr = case head gr of
            1 -> tribonacci !! (length gr + 2)
            3 -> 1
            _ -> error "oops"
        factors = map f groups
    in product factors
    


main :: IO ()
main = do
    s <- readFile "input.txt"
    let l = map read $ lines s
    let diff = joltageDifferences l
    let ones = length $ filter (==1) diff
    let threes = length $ filter (==3) diff
    
    print $ ones * threes

    print $ getNumOfArrangements l
