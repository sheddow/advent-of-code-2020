module Main where

import Lib

main :: IO ()
main = do
    s <- readFile "input.txt"
    let m = stringToMap s
    print $ countTrees (0, 0) m
    print $ product $ map (\(r, d) -> countTrees' r d (0, 0) m) [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
