import Test.Hspec

import Lib


example_map :: IO Map
example_map =
    let s = readFile "test_input.txt"
    in fmap stringToMap s


spec :: Spec
spec = do
    describe "countTrees" $ do
        it "works" $ fmap (countTrees (0, 0)) example_map `shouldReturn` 7
    describe "countTrees'" $ do
        it "works" $ fmap (countTrees' 1 1 (0, 0)) example_map `shouldReturn` 2
        it "works" $ fmap (countTrees' 3 1 (0, 0)) example_map `shouldReturn` 7
        it "works" $ fmap (countTrees' 5 1 (0, 0)) example_map `shouldReturn` 3
        it "works" $ fmap (countTrees' 7 1 (0, 0)) example_map `shouldReturn` 4
        it "works" $ fmap (countTrees' 1 2 (0, 0)) example_map `shouldReturn` 2


main :: IO ()
main = hspec spec
