module Lib
    ( Square
    , Map
    , Position
    , stringToMap
    , countTrees
    , countTrees'
    ) where


data Square = Empty | Tree deriving Enum

type Map = [[Square]]

type Position = (Int, Int)


-- TODO: return Maybe Square?
charToSquare :: Char -> Square
charToSquare '#' = Tree
charToSquare '.' = Empty


expandMapRightwards :: Map -> Map
expandMapRightwards = map cycle


stringToMap :: String -> Map
stringToMap = expandMapRightwards . (map . map) charToSquare . lines


countTrees' :: Int -> Int -> Position -> Map -> Int
countTrees' right down (row, col) m
    | row >= length m = 0
    | otherwise =
        let square = (m !! row) !! col
        in (fromEnum square) + countTrees' right down (row + down, col + right) m


countTrees :: Position -> Map -> Int
countTrees = countTrees' 3 1
