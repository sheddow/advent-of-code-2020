import Data.Maybe (catMaybes)
import Data.List (find)

import qualified Data.Set as Set


headMaybe :: [a] -> Maybe a
headMaybe (x:_) = Just x
headMaybe [] = Nothing


findNumsThatSums :: Int -> Set.Set Int -> Maybe (Int, Int)
findNumsThatSums sum set = fmap (\x -> (x, sum-x)) $ find (\x -> (sum-x) `Set.member` set) set


findThreeNumsThatSums :: Int -> Set.Set Int -> Maybe (Int, Int, Int)
findThreeNumsThatSums sum set =
    let xs = catMaybes $ map (\x -> findNumsThatSums (sum-x) set) (Set.toList set)
    in fmap (\(x, y) -> (sum - x - y, x, y)) (headMaybe xs)


main :: IO ()
main = do
    s <- readFile "input.txt"
    let xs = map read (lines s)
        set = Set.fromList xs
        Just (x, y) = findNumsThatSums 2020 set
    print $ x*y
    let Just (x, y, z) = findThreeNumsThatSums 2020 set
    print $ x*y*z
