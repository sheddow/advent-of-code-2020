newtype Stream a = Stream { getNonEmpty :: NonEmpty a }


instance Functor Stream where
--  fmap :: (a -> b) -> f a -> f b
    fmap f s = Stream $ fmap f (getNonEmpty s)


instance Comonad Stream where
--  extract :: w a -> a
    extract s = NonEmpty.head $ getNonEmpty s
--  duplicate :: w a -> w (w a)
    duplicate s =
        let (x :| xs) = getNonEmpty s
        in case nonEmpty xs of
            Nothing -> Stream $ s :| []
            Just tail -> Stream $ s `cons` getNonEmpty (duplicate (Stream tail))


-- proof of comonad laws:
-- goal: extract . duplicate = id
--
-- extract (duplicate s) =
-- NonEmpty.head $ getNonEmpty (duplicate s) =
-- NonEmpty.head $ getNonEmpty (s :| [])  or   NonEmpty.head $ getNonEmpty (s `cons` ...)
-- NonEmpty.head (s `cons` ...) = s
--
--
-- goal: fmap extract . duplicate = id
-- fmap extract (duplicate s) =
-- extract (head $ duplicate s) `cons` fmap extract (tail $ duplicate s)  # definition of fmap
-- extract s `cons` fmap extract (tail $ duplicate s)  # head (duplicate s) = s, as proved above
-- extract s `cons` fmap extract (duplicate (tail s))  # kinda definition of duplicate
-- extract s `cons` tail s                             # inductive hypothesis
-- s                                                   # head x `cons` tail x == x
--
--
-- goal: fmap duplicate . duplicate = duplicate . duplicate
-- fmap duplicate (duplicate s) =
-- duplicate (head $ duplicate s) `cons` fmap duplicate (tail $ duplicate s)
-- duplicate s `cons` fmap duplicate (tail $ duplicate s)
-- duplicate s `cons` fmap duplicate (duplicate (tail s))
-- duplicate s `cons` duplicate (duplicate (tail s))
-- ???
-- QED
--
