import Control.Monad.State
import Data.List

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

import qualified Data.List.NonEmpty as NonEmpty
import Data.List.NonEmpty (cons, NonEmpty(..))



allNumbers :: State (NonEmpty Int) [Int]
allNumbers = do
    nums@(x :| xs) <- get
    let n = case elemIndex x xs of 
            Nothing -> 0
            Just idx -> idx + 1
    put (n `cons` nums)
    rest <- allNumbers
    return $ n : rest


getNumbers :: [Int] -> [Int]
getNumbers startnums = startnums ++ evalState allNumbers (NonEmpty.fromList (reverse startnums))



allNumbersFast :: State (Int, Int, Map Int Int) [Int]
allNumbersFast = do
    (idx, cur, m) <- get
    let next = case Map.lookup cur m of
            Nothing -> 0
            Just i -> idx - i
    put (idx+1, next, Map.insert cur idx m)
    rest <- allNumbersFast
    return $ next : rest


getNumbersFast :: [Int] -> [Int]
getNumbersFast startnums =
    let end = last startnums
        m = Map.fromList $ zip (init startnums) [0..]
    in startnums ++ evalState allNumbersFast (length startnums - 1, end, m)


main :: IO ()
main = do
    let startnums = [20,9,11,0,1,2]
    print $ (getNumbers startnums) !! (2020-1)
    --print $ (getNumbersFast startnums) !! (3000000-1)
