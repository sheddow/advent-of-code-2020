use std::env;

use fnv::FnvHashMap;


struct NumberGen {
    idx: u32,
    cur: u32,
    map: FnvHashMap<u32, u32>,
}


impl NumberGen {
    fn new(startnums: &[u32]) -> Self {
        if startnums.len() == 0 {
            panic!("no start numbers, can't start")
        }
        let end = startnums.last().unwrap();
        let startnums_init = &startnums[0..startnums.len()-1];
        NumberGen {
            idx: startnums.len() as u32 - 1,
            cur: *end,
            map: startnums_init.into_iter().copied().zip(0..).collect(),
        }
    }
}


impl Iterator for NumberGen {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        let next_num = match self.map.get(&self.cur) {
            None => 0,
            Some(prev_idx) => self.idx - prev_idx,
        };

        self.map.insert(self.cur, self.idx);

        self.cur = next_num;
        self.idx += 1;

        Some(next_num)
    }
}



fn main() {
    let startnums: Vec<u32> = env::args().skip(1).map(|s| s.parse().unwrap()).collect();

    let gen = NumberGen::new(&startnums);

    println!("{}", gen.skip(30000000-(startnums.len())-1).next().unwrap());
}
