import Data.Foldable
import Data.Maybe
import Control.Monad.State

import Data.Sequence hiding (zip, filter, splitAt, drop)
import qualified Data.Sequence as Seq


isSumOfTwoNumbers :: Seq Int -> Int -> Bool
isSumOfTwoNumbers s x = isJust $ find (\(idx, y) -> (x - y) `elem` (deleteAt idx s)) (fromList (zip [0..] (toList s)))



isSumOfPreceding :: Int -> State (Seq Int) (Maybe Int)
isSumOfPreceding x = do
    buf <- get
    let buf' = (Seq.drop 1 buf) |> x
    put buf'
    if (not $ isSumOfTwoNumbers buf x) then
        return $ Just x
    else do
        return Nothing


-- does this always work??
runningSum :: (Seq Int) -> (Seq Int) -> Int -> Maybe (Seq Int)
runningSum buf rest target
    | sum buf == target = Just buf
    | sum buf > target = runningSum (Seq.drop 1 buf) rest target
    | otherwise = case rest of
        (x :<| xs) -> runningSum (buf |> x) xs target
        _ -> Nothing


main :: IO ()
main = do
    s <- readFile "input.txt"
    let numbers = map read $ lines s
    let (preamble, xs) = splitAt 25 numbers
    let res = head $ catMaybes $ evalState (mapM isSumOfPreceding xs) (fromList preamble)
    print $ res

    let Just res2 = runningSum Seq.empty (fromList numbers) res
    print $ (minimum res2) + (maximum res2)
