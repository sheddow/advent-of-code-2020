module Main where

import Lib

import Data.Sequence (fromList)
import qualified Data.Sequence as Seq
import Data.Maybe (catMaybes)


main :: IO ()
main = do
    s <- readFile "input.txt"
    let instructions = strToInstructions s
    let l = fromList $ map (\i -> (i, False)) instructions
    print $ runUntilLoop l 0 0

    let possibleInstructions = uncorruptInstructions (fromList instructions)
    let l' = map (fmap (\i -> (i, False))) possibleInstructions
    let res = map (\i -> runUntilFinished i 0 0) l'
    print $ catMaybes res
