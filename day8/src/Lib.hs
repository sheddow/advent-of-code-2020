module Lib
    ( Instruction
    , strToInstructions
    , runUntilLoop
    , runUntilFinished
    , uncorruptInstructions
    ) where


import Data.Foldable (toList)

import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Text.Parsec
import Text.Parsec.String (Parser)


data Instruction = Acc Int | Jmp Int | Nop Int deriving Eq


parseInt :: Parser Int
parseInt = do
    -- todo: sign and number
    sign <- char '-' <|> char '+'
    num <- fmap read $ many1 digit
    case sign of
        '-' -> return (-num)
        '+' -> return num


parseInstruction :: Parser Instruction
parseInstruction = do
    inst <- string "acc" <|> string "jmp" <|> string "nop"
    space
    case inst of
        "acc" -> fmap Acc parseInt
        "jmp" -> fmap Jmp parseInt
        "nop" -> fmap Nop parseInt



strToInstructions :: String -> [Instruction]
strToInstructions s =
    let unwrap (Right x) = x
    in map (unwrap. parse parseInstruction "") (lines s)



runInstruction :: Instruction -> Int -> Int -> (Int, Int)
runInstruction (Nop _) idx acc = (idx + 1, acc)
runInstruction (Acc x) idx acc = (idx + 1, acc + x)
runInstruction (Jmp x) idx acc = (idx + x, acc)


runUntilLoop :: Seq (Instruction, Bool) -> Int -> Int -> Int
runUntilLoop instructions idx acc =
    let (inst, visited) = instructions `Seq.index` idx
    in if visited then
        acc
    else 
        let instructions' = Seq.update idx (inst, True) instructions
            (idx', acc') = runInstruction inst idx acc
        in runUntilLoop instructions' idx' acc'


runUntilFinished :: Seq (Instruction, Bool) -> Int -> Int -> Maybe Int
runUntilFinished instructions idx acc =
    if idx >= Seq.length instructions then
        if idx == Seq.length instructions then
            Just acc
        else
            Nothing
    else
        let (inst, visited) = instructions `Seq.index` idx
        in if visited then
            Nothing
        else 
            let instructions' = Seq.update idx (inst, True) instructions
                (idx', acc') = runInstruction inst idx acc
            in runUntilFinished instructions' idx' acc'


swap :: Instruction -> Instruction
swap (Jmp x) = (Nop x)
swap (Nop x) = (Jmp x)
swap x = x


uncorruptInstructions :: Seq Instruction -> [Seq Instruction]
uncorruptInstructions instructions =
    let l = filter (\(idx, inst) -> case inst of (Acc _) -> False; _ -> True) (zip [0..] (toList instructions))
    in map (\(idx, inst) -> Seq.update idx (swap inst) instructions) l


