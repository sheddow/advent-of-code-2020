module Main where

import Lib

main :: IO ()
main = do
    s <- readFile "input.txt"
    let l = lines s
    print $ length $ filter ((uncurry passesPolicy) . stringToPolicyAndPass) l
    print $ length $ filter ((uncurry passesPolicy2) . stringToPolicyAndPass) l

