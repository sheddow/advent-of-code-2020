import Test.Hspec

import Lib


spec :: Spec
spec = do
    describe "stringToPolicyAndPass" $ do
        it "parses a string" $ stringToPolicyAndPass "1-3 a: abcde" `shouldBe` (Policy {min'=1, max'=3, c='a'}, "abcde")
    describe "passesPolicy" $ do
        it "works" $ passesPolicy (Policy 1 3 'a') "abcde" `shouldBe` True
        it "works very well" $ passesPolicy (Policy 1 3 'b') "cdefg" `shouldBe` False
    describe "passesPolicy2" $ do
        it "works" $ passesPolicy2 (Policy 1 3 'a') "abcde" `shouldBe` True
        it "works indeed" $ passesPolicy2 (Policy 1 3 'b') "cdefg" `shouldBe` False
        it "works oh yes" $ passesPolicy2 (Policy 2 9 'c') "ccccccccc" `shouldBe` False


main :: IO ()
main = hspec spec
