{-# LANGUAGE NamedFieldPuns #-}

module Lib
    ( Policy (..)
    , passesPolicy
    , passesPolicy2
    , stringToPolicyAndPass
    ) where

import Text.ParserCombinators.ReadP
import Data.Char (isDigit)


data Policy = Policy { min' :: Int, max' :: Int, c :: Char } deriving (Eq, Show)


intReader :: ReadP Int
intReader = fmap read $ many1 (satisfy isDigit)


parsePolicy :: ReadP Policy
parsePolicy = do
    min' <- intReader
    char '-'
    max' <- intReader
    char ' '
    c <- get
    return $ Policy { min', max', c }


parsePolicyAndPassword :: ReadP (Policy, String)
parsePolicyAndPassword = do
    policy <- parsePolicy
    char ':' >> char ' '
    pass <- look
    return (policy, pass)


stringToPolicyAndPass :: String -> (Policy, String)
stringToPolicyAndPass s = fst $ head $ readP_to_S parsePolicyAndPassword s


passesPolicy :: Policy -> String -> Bool
passesPolicy (Policy {min', max', c}) s =
    let occurrences = length $ filter (==c) s
    in min' <= occurrences && occurrences <= max'


passesPolicy2 :: Policy -> String -> Bool
passesPolicy2 (Policy {min'=idx1, max'=idx2, c}) s =
    let cs = [s !! (idx1 - 1), s !! (idx2 - 1)]
    in length (filter (==c) cs) == 1
