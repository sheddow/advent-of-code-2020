import Data.List.Split (splitOn)
--import Data.List (transpose, intersect, (\\))
import Data.List
import Data.Function (on)

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String (Parser)


data Rule = Rule String [(Int, Int)] deriving (Eq, Show)

type Ticket = [Int]


parseRange :: Parser (Int, Int)
parseRange = do
    lo <- fmap read $ many1 digit
    char '-'
    hi <- fmap read $ many1 digit
    return (lo, hi)


parseRule :: Parser Rule
parseRule = do
    name <- manyTill anyChar (char ':')
    space
    ranges <- sepBy parseRange (string " or ")
    return $ Rule name ranges



followsRule :: Rule -> Int -> Bool
followsRule (Rule _ ranges) field = any (\(l, h) -> l <= field && field <= h) ranges

followsAnyRule :: [Rule] -> Int -> Bool
followsAnyRule rules field = any (\rule -> followsRule rule field) rules


getInvalidFields :: [Rule] -> Ticket -> [Int]
getInvalidFields rules ticket = filter (\field -> not (followsAnyRule rules field)) ticket


isValidTicket :: [Rule] -> Ticket -> Bool
isValidTicket rules ticket = all (followsAnyRule rules) ticket


getPossibleFields :: [Rule] -> Int -> [Rule]
getPossibleFields rules field = filter ((flip followsRule) field) rules


getPossibleFieldsInCommon :: [Rule] -> [Int] -> [Rule]
getPossibleFieldsInCommon rules fields = foldr1 intersect $ map (getPossibleFields rules) fields


-- doesn't work for lists with empty elements
reduceByElimination :: Eq a => [(Int, [a])] -> [(Int, a)]
reduceByElimination [] = []
reduceByElimination l =
    let (singles, rest) = partition ((==1) . length . snd) l
        r = map (\(a, b) -> (a, head b)) singles
        eliminated = map (\(i, x) -> (i, x \\ (map snd r))) rest
    in r ++ reduceByElimination eliminated


unwrap :: Either a b -> b
unwrap (Right x) = x


main :: IO ()
main = do
    s <- readFile "input.txt"
    let (rules_str:s1:s2:[]) = splitOn "\n\n" s

    let rules = map (unwrap . parse parseRule "") (lines rules_str)

    let nearby_tickets = map (map read . splitOn ",") (tail $ lines s2)

    print $ sum $ map (sum . getInvalidFields rules) nearby_tickets

    let valid_tickets = filter (isValidTicket rules) nearby_tickets :: [[Int]]
        names_and_indices = reduceByElimination $ zip [0..] (map (getPossibleFieldsInCommon rules) (transpose valid_tickets))
        relevant_names_and_indices = filter (\(_, Rule name _) -> isPrefixOf "departure" name) names_and_indices

    let myticket = map read $ splitOn "," ((lines s1) !! 1)
    let myfields = map (\(idx, _) -> myticket !! idx) relevant_names_and_indices

    print $ product myfields
