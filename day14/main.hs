import Data.Bits
import Data.Char (digitToInt)
import Data.List (foldl')

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String (Parser)

import qualified Data.Map.Strict as Map


type Mask = String

data Instruction = SetMask Mask | WriteToMem Int Int


replace :: Char -> Char -> String -> String
replace c1 c2 = map (\x -> if x == c1 then c2 else x)

replaceM :: (Eq a, Monad m) => a -> m a -> [a] -> m [a]
replaceM c1 c2 = mapM (\x -> if x == c1 then c2 else return x)

applyMask :: Mask -> Int -> Int
applyMask mask n =
    let zero_mask = binToInt $ replace 'X' '1' mask
        ones_mask = binToInt $ replace 'X' '0' mask
    in (n .&. zero_mask) .|. ones_mask


binToInt :: String -> Int
binToInt = foldl' (\acc x -> acc * 2 + digitToInt x) 0


parseInstruction :: Parser Instruction
parseInstruction = do
    s <- try (string "mask = ") <|> string "mem["
    case s of
        "mask = " -> fmap SetMask $ many1 (oneOf "10X")
        "mem[" -> do
            addr <- fmap read (many1 digit)
            string "] = "
            val <- fmap read (many1 digit)
            return $ WriteToMem addr val



runInstruction :: Instruction -> (Mask, Map.Map Int Int) -> (Mask, Map.Map Int Int)
runInstruction (SetMask mask) (_, m) = (mask, m)
runInstruction (WriteToMem addr val) (mask, m) =
    let m' = Map.insert addr (applyMask mask val) m
    in (mask, m')


runInstruction' :: Instruction -> (Mask, Map.Map Int Int) -> (Mask, Map.Map Int Int)
runInstruction' (SetMask mask) (_, m) = (mask, m)
runInstruction' (WriteToMem addr val) (mask, m) =
    let masks = map (replace 'Y' 'X') $ replaceM 'X' ['1', '0'] (replace '0' 'Y' mask)
        updates = map (\mask -> Map.insert (applyMask mask addr) val) masks
        new_map = foldr1 (.) updates $ m
    in (mask, new_map)


unwrap :: Show a => Either a b -> b
unwrap (Right x) = x
unwrap (Left x) = error (show x)


main :: IO ()
main = do
    s <- readFile "input.txt"
    let instructions = map (runInstruction . unwrap . parse parseInstruction "") (lines s)

    let inst = foldr1 (flip (.)) instructions

    let (_, mem) = inst ("X", Map.empty)

    print $ sum mem

    
    let instructions' = map (runInstruction' . unwrap . parse parseInstruction "") (lines s)
    let inst' = foldr1 (flip (.)) instructions'

    let (_, mem') = inst' ("X", Map.empty)

    print $ sum mem'
