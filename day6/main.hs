import qualified Data.Set as Set

import Data.List.Split


countAnswersInGroup :: String -> Int
countAnswersInGroup s = length $ Set.fromList $ filter (`elem` ['a'..'z']) s


countUnanimousAnswers :: String -> Int
countUnanimousAnswers s =
    let shared = foldr1 Set.intersection $ map Set.fromList (lines s)
    in length $ shared


main :: IO ()
main = do
    s <- readFile "input.txt"
    let groups = splitOn "\n\n" s
    print $ sum $ map countAnswersInGroup groups
    print $ sum $ map countUnanimousAnswers groups
