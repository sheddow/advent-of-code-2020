module Lib
    ( parseRule
    , strToRules
    , strToRules'
    , getBagAncestors
    , countBagChildren
    ) where


import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.String (Parser)

import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust)


noBags :: Parser [(Int, String)]
noBags = do
    string "no other bags"
    return []


oneBag :: Parser String
oneBag = do
    bag <- manyTill (letter <|> space) (try (string " bag" >> optional (char 's')))
    return bag


numBag :: Parser (Int, String)
numBag = do
    num <- many1 digit
    space
    bag <- oneBag
    return (read num, bag)


parseRule :: Parser (String, [(Int, String)])
parseRule = do
    bag <- oneBag
    string " contain "
    bags <- noBags <|> sepBy numBag (string ", ")
    return (bag, bags)



strToRules :: String -> [(String, [String])]
strToRules s =
    let l = lines s
        f (a, l) = (a, map snd l)
        unwrap (Right x) = x
    in map (f . unwrap . parse parseRule "") l


strToRules' :: String -> Map.Map String [(Int, String)]
strToRules' s =
    let unwrap (Right x) = x
    in Map.fromList $ map (unwrap . parse parseRule "") (lines s)



getBagAncestors :: [(String, [String])] -> String -> Set.Set String -> Set.Set String
getBagAncestors rules bag running_set =
    let bag_parents = map fst $ filter ((bag `elem`) . snd) rules
        running_set' = running_set `Set.union` Set.fromList bag_parents
        grand_parents = map (\p -> if p `elem` running_set then Set.singleton p else getBagAncestors rules p running_set') bag_parents
    in foldr Set.union (Set.singleton bag) grand_parents



countBagChildren :: Map.Map String [(Int, String)] -> String -> Int
countBagChildren rules bag =
    let children = fromJust $ Map.lookup bag rules
    in sum $ map (\(count, b) -> count + count * (countBagChildren rules b)) children
