module Main where

import Lib

import qualified Data.Set as Set


main :: IO ()
main = do
    s <- readFile "input.txt"
    let rules = strToRules s
    let bag_ancestors = getBagAncestors rules "shiny gold" Set.empty
    print $ (length bag_ancestors) - 1
    let rules' = strToRules' s
    print $ countBagChildren rules' "shiny gold"
