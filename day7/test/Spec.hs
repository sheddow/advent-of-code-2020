import Lib

import Test.Hspec
import Text.Parsec (parse)

import qualified Data.Set as Set
import qualified Data.Map.Strict as Map


test_rules :: IO [(String, [String])]
test_rules = fmap strToRules $ readFile "test_input.txt"

test_rules' :: IO (Map.Map String [(Int, String)])
test_rules' = fmap strToRules' $ readFile "test_input.txt"

spec :: Spec
spec = do
    describe "parseRule" $ do
        it "works" $ parse parseRule "" "bright white bags contain 1 shiny gold bag." `shouldBe` Right ("bright white", [(1, "shiny gold")])
        it "works" $ parse parseRule "" "faded blue bags contain no other bags." `shouldBe` Right ("faded blue", [])
        it "works" $ parse parseRule "" "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags." `shouldBe` Right ("vibrant plum", [(5, "faded blue"), (6, "dotted black")])
    describe "getBagAncestors" $ do
        it "works" $ fmap (\rules -> getBagAncestors rules "shiny gold" Set.empty) test_rules `shouldReturn` Set.fromList ["bright white", "muted yellow", "dark orange", "light red", "shiny gold"]
    describe "countBagChildren" $ do
        it "works" $ fmap (\rules -> countBagChildren rules "shiny gold") test_rules' `shouldReturn` 32


main :: IO ()
main = hspec spec
